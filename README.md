## tiare-user 8.1.0 OPM1.171019.026 V10.2.19.0.OCLINXM release-keys
- Manufacturer: xiaomi
- Platform: 
- Codename: tiare
- Brand: Xiaomi
- Flavor: lineage_tiare-eng
- Release Version: 9
- Id: PQ3A.190801.002
- Incremental: eng.sinha_.20220201.140659
- Tags: test-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Xiaomi/tiare_in/tiare:8.1.0/OPM1.171019.026/V10.2.19.0.OCLINXM:user/release-keys
- OTA version: 
- Branch: tiare-user-8.1.0-OPM1.171019.026-V10.2.19.0.OCLINXM-release-keys
- Repo: xiaomi_tiare_dump_22428


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
